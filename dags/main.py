import json
from datetime import datetime

import requests
from pymongo import MongoClient

api_key = 'f7e5f33ce37d3518345d2c2886a5c665'
base_url = 'https://financialmodelingprep.com'

rating_endpoint = 'api/v3/rating/AAPL'
profile_endpoint = 'api/v3/profile/AAPL'


def retrieve_data() -> None:
    ratings = requests.get(f'{base_url}/{rating_endpoint}?apikey={api_key}').json()[0]
    profile = requests.get(f'{base_url}/{profile_endpoint}?apikey={api_key}').json()[0]
    data = {'ratings': ratings, 'profile': profile, 'timestamp': str(datetime.now())}

    with open('/tmp/data.json', 'w') as f:
        json.dump(data, f)

    return data

def insert_to_db() -> None:
    with open('/tmp/data.json', 'r') as f:
        data = json.load(f)

    print(data)

    mongo_client = MongoClient("mongodb://root:root@mongo:27017/")
    db = mongo_client["db"]
    collection = db["financial"]

    x = collection.insert_one(data)
    print(x.inserted_id)

    cursor = collection.find({})
    for document in cursor:
        print(document)