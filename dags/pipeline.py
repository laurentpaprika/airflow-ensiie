from datetime import timedelta

import airflow
from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from main import retrieve_data
from jsontomongo import JsonToMongoOperator

args = {
    'id': 2,
    'owner': 'maxime',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    'starter',
    default_args=args,
    schedule_interval=timedelta(minutes=1),
    catchup=False

)

t1 = PythonOperator(
    task_id='t1',
    python_callable=retrieve_data,
    dag=dag
)

t2 = JsonToMongoOperator(
    task_id='t2',
    file_to_load='/tmp/data.json',
    mongoserver='mongo',
    mongopass='root',
    mongouser='root',
    dag=dag
)

t1 >> t2