import json
from airflow.models import DagBag
from os import path
from airflow.operators.python_operator import PythonOperator
from main import retrieve_data
from jsontomongo import JsonToMongoOperator

def test_python_operator():
    test = PythonOperator(task_id='test',python_callable=retrieve_data)
    result = test.execute(context={})
    assert 'ratings' in result
    assert 'profile' in result

def test_mongoopertor():
    test = PythonOperator(task_id='test',python_callable=retrieve_data)
    result = test.execute(context={})
    test2 = JsonToMongoOperator(
        task_id='test2',
        file_to_load='/tmp/data.json',
        mongoserver='mongo',
        mongopass='root',
        mongouser='root')
    result = test2.execute(context={})
    assert result > 0